PS1="\e[90m\u@\h \$PWD $\e[0m "

alias v="vim "
alias e="exit"

alias vimrc="vim ~/.config/nvim/init.vim"
alias bashrc="vim ~/.bashrc; source ~/.bashrc"

# alias xterm="xterm -fg lightgray -bg black -cr magenta -fn terminus-14"

# alias octave="octave-cli --silent"

# sxhkd
# entr

# source ~/scripts/sh/clear_dot_files.sh
