" setting hibrid (relative and absolute) numbers
set relativenumber
set nu rnu

" no highlighed search
set nohlsearch

" remove all the extra files
set nobackup
set nowritebackup
set noswapfile

" remove autoindentation
set noautoindent
set nocindent
set nosmartindent
filetype indent off

" remove autocomments
set formatoptions-=cro

" use tab for autocompletition
imap <Tab> <C-N>

" exit from insert mode
imap jj <Esc>
imap hh <Esc>
imap kk <Esc>
imap ii <Esc>

" use I for append
nmap I a

" navigation end of line with capital L and H
nmap L $
nmap H 0
vmap L $
vmap H 0

" fast navigation up and down with J and K 
nmap J <C-d>zz
nmap K <C-u>zz

" quit and write with Q and W
nmap Q :q!<Enter>
nmap W :w<Enter>

" make new line with o and 0
nmap O 0i<Enter><Esc>
nmap o $a<Enter><Esc>k

" search the current word with Cntr N
nmap <C-N> ye/<C-R>0
nmap <C-A-N> ye?<C-R>0

" disable Cntr z
nnoremap <C-z> <NOP>

set termguicolors
colorscheme base16-grayscale-dark

