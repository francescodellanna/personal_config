\centerline{\textbf{\Large{
FRANCESCO GIUSEPPE DELL' ANNA
}}}

\vspace{5mm}

\small{
\centerline{
Email: fd@francescodellanna.xyz - Phone: 0047 47722835 
}

\centerline{
Git: https://gitlab.com/francescodellanna - Blog: https://fdblog.xyz
}
}

------------------------------------------------------------------------

\normalsize{
With great amenability, I want to apply for a Software Platform Engineer position at Graphcore. 
I am firmly confident that my professional profile including: education, working experience, skill-set and professional training makes me a suitable candidate for the position.

Here an overview of my education and professional career so far.

I attained a degree in Embedded Systems from Politecnico di Torino in July 2016 with a final score of 109/110L. 
I spent the last year of master at KULeuven university (Belgium) where I worked on my master thesis in IMEC developing an interface between two neuromorphic simulators, CARLsim and Noxim, mapping neurons on hardware clusters. The interface was developed in C++.

As an individual who is interested in continuous professional development, after the degree I worked as a researcher in CTBU. 
This has resulted in professional competency in analogue electronics, and it gave me a complete overview and confidence on the main aspects of the academic research world.

The research work pertains the development of autonomous wireless sensor nodes powered by piezoelectric energy harvesting technology. The job emplacement is primarily in Norway (USN) and China (CTBU) with regular relocations within the two nations.

After my research activity, I worked as a digital designer engineer for both Omnivision and Sony on automotive image sensors.
I contributed in the development of the following automotive sensors: OX8A OX8B and OX3C for Omnivision and IMX728, ISX728 for Sony.
I workekd on several tasks as digital designer, RTL development, verification and backend tasks.
Furtermore, I actively developed new IPs and algorithms for imaging that are or will be implemented in industrial products.

I am aware of the relevance of AI today and, more importantly, its potential future.
Therefore, I would like to be part of this shift in computing paradigm, building the hardware-software infrastructure to make AI more accessible.

I look forward to hear back from you after appraisal of my resume. I am an individual with great potential and enthusiasm with a lot of willingness to work in an organization such as Graphcore.

Sincerely,

Francesco Giuseppe Dell’Anna
}


