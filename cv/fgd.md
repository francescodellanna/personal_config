<div id="header" class="paragraph text-center">

\centerline{\textbf{\Large{
FRANCESCO GIUSEPPE DELL' ANNA
}}}

\vspace{5mm}

\small{
\centerline{
Email: fd@francescodellanna.xyz - Phone: 0047 47722835 
}

\centerline{
Git: https://gitlab.com/francescodellanna\ - Blog: https://fdblog.xyz
}
}

------------------------------------------------------------------------


## Technical Strengths

*Programming languages* - Verilog, Posix Shell, Latex, Python,
Octave, C, C++,


------------------------------------------------------------------------


## Industrial Experience


### 2019 - Current : Digital Designer at Sony 

Development of a novel image compression algorithm, configuration tool development, and design of temperature and power modules for IMX728 ISX728 image sensors. Configuration tool development.


### 2017 - 2019 : Digital Designer at Omnivision Technology


Design development and testing of automotive image sensors (OX8a OX8b Ox3c). I actively designed and tested of multiple modules comprising the input and output data-path, as well as modules implementing built-in tests (Watchdog, PMBIST, SBIST).


------------------------------------------------------------------------

## Academical Research


### 2016 - 2018 : CTBU and USN Researcher

\small{

-   IC design for a piezoelectric harvesting interface based on a frequency up-conversion technique.

-   Design of a synthesis tool for preliminary topology identification of a low-power voltage multiplier.
}

### Granted projects 

\small{
-   Chongqing Key Laboratory of Micro-Nanosystems Technology and Smart Transducing no. KFJJ2017087.
}

### List of publications

\small{
-   Francesco Dell' Anna, Tao Dong, Ping Li, Wen Yumei, Zhaochu Yang, Mario R. Casu, Mehdi Azadmehr and Yngvar Berg, State-of-the-art power management circuits for piezoelectric energy harvesters. IEEE Circuits and Systems Magazine, DOI: 10.1109.

-   Francesco Dell' Anna, Tao Dong, Ping Li, Wen Yumei, Mehdi Azadmehr, Mario R. Casu and Yngvar Berg, Lower-order compensation chain threshold-reduction technique for multi-stage voltage multipliers, Sensors, DOI:10.3390.

-   Francesco G. Dell Anna, Tao Dong, Ping Li, Wen Yumei, Mehdi Azadmehr and Yngvar Berg Low-power voltage multiplier synthesis tool for preliminary topology identification., Conference in IEEE ICOSST, DOI: 10.1109.

-   Anup Das, Yuefeng Wu, Khanh Huynh, Francesco Dell Anna, Francky Catthoor and Siebren Schaafsma, ”Mapping of Local and Global Synapses on Spiking Neuromorphic Hardware”, Conference on Design Automation and Test in Europe, DOI: 10.23919.

-   Wenjun Lin, Xuefu Xu, Francesco DellAnna, ”The impact of economic plans on the Chinese education system: a machine learning approach, DOI: 10.3280 CADMO.

-   Adarsha Balaji, Anup Das, Yuefeng Wu, Khanh Huynh, Francesco DellAnna, Giacomo Indiveri, Jeff Krichmar, Nikil Dutt, Siebren Schaafsma, and Francky Catthoor, ”Mapping Spiking Neural Networks on Neuromorphic Hardware”, IEEE Transactions on Very Large Scale Integration 2019, DOI: 10.1109.
}

------------------------------------------------------------------------

## Education 


### 2015 - 2016 : IMEC Master Thesis

Interface between RTL and application neuromorphic simulator.


### 2015 - 2016 : KULeuven Electronics and Integrated Circuits


Power and propagation delay estimation in digital circuits, hardware software co-design.

### 2014 - 2016 : Master in Embedded Systems


Fundamental knowledge in embedded systems both software and hardware, digital design and processor architectures.

### 2011 - 2013 : Bachelor in Embedded Systems

Fundamental knowledge in computer architectures and algorithms.


## Few words about me

I am an engineer eager to share my personal experience and views on technical problems. I am originally from Italy but, thanks to my profession, I had the opportunity to live in Belgium, China and I am currently in Norway. 

I am a highly creative person. What motivates me is to compete with state-of-art technology, and coming up with new ideas which can push industry and research forward. Algorithms are my passion, because they focus my attention on what is the problem and what to do to solve it.

Other hobbies outside the industry: board game design, create and submit board games to publishers, music, I play guitar and piano, and I love to go for walks in nature.
