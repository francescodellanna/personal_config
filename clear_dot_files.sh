#!bin/dash

rm -rf ~/.fltk
# rm -f ~/Digital_*
# rm -f ~/octave-workspace*

ls -a | column -t          | \
awk "/^\./" | awk "!/\.$/" | \
grep -v bashrc             | \
grep -v profile            | \
grep -v config             | \
grep -v local              | \
xargs rm -rf 